package INF102.lab3.numberGuesser;


public class MyGeniusGuesser implements IGuesser {

	@Override
    public int findNumber(RandomNumber number) {
        int lower = number.getLowerbound();
        int upper = number.getUpperbound();
        int pivot = 32; // tmp
        while (number.guess(pivot) != 0) {
            pivot = (lower + upper) / 2;
            
            if (number.guess(pivot) == 0) { return pivot;} 
            else if (number.guess(pivot) == -1) {lower = pivot;} 
            else if (number.guess(pivot) == 1) {upper = pivot;} 
            else {break;}
        }
        return pivot;
    }

}
