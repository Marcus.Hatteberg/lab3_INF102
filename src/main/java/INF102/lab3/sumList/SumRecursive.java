package INF102.lab3.sumList;

import java.util.List;

public class SumRecursive implements ISum {

    @Override
    public long sum(List<Long> list) {
        if(list.size() == 0){return 0;}

        List<Long> tmp = list.subList(1, list.size());
        return list.get(0) + sum(tmp);
    }
    

}
