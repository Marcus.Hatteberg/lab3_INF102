package INF102.lab3.peakElement;

import java.util.List;

public class PeakRecursive implements IPeak {

    public static int helperPeakElement(List<Integer> numbers, int candidate_left, int candidate_right)
    {
        int candidate = (candidate_left + candidate_right) / 2;
 
        if ((candidate == 0 
            || numbers.get(candidate - 1) <= numbers.get(candidate)) 
            && (candidate == numbers.size() - 1 
            || numbers.get(candidate + 1) <= numbers.get(candidate))) 
        {
            return candidate;
        }
 
        if (candidate - 1 >= 0 && numbers.get(candidate - 1) > numbers.get(candidate)) 
        {
            return helperPeakElement(numbers, candidate_left, candidate - 1);
        }
 
        return helperPeakElement(numbers, candidate + 1, candidate_right);
    }

    @Override
    public int peakElement(List<Integer> numbers) {
        if (numbers == null || numbers.size() == 0) {return 0;}

        int index = helperPeakElement(numbers, 0, numbers.size() - 1);
        return numbers.get(index);
    }

}
